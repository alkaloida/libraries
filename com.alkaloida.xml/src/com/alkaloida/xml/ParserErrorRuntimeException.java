/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.xml;

/**
 *
 * @author szlelesz
 */
public class ParserErrorRuntimeException extends RuntimeException{
    private static final long serialVersionUID = 1L;
  public ParserErrorRuntimeException(Throwable cause){
      super(cause);
  }
}
